﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Nordwind.Mvc.Extensions
{
    public static class StringExtensions
    {
        public static string Slugify(this string value)
        {
            return Regex.Replace(value.ToLower(), @"[^\d\w]", "-");
        }
    }
}
