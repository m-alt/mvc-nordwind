﻿using Nordwind.Data.Domain.Repository;
using Nordwind.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nordwind.Mvc.Filter
{
    public class InjectingCategoriesAttribute : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var repository = new CategoryRepository(new NordwindCtx());
            filterContext.Controller.ViewBag.Categories = repository.GetAll();
        }
    }
}
