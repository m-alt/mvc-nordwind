﻿using Nordwind.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nordwind.Data.Domain.Repository
{
    public class ProductRepository
    {
        NordwindCtx db;

        public ProductRepository(NordwindCtx db)
        {
            this.db = db;
        }

        public IEnumerable<ProductInfo> GetRandomProducts(int count = 3)
        {
            var products = db.Products.SqlQuery("SELECT TOP 3 * FROM Products WHERE discontinued <> 1 ORDER BY NEWID()");

            return products.Select(p => new ProductInfo() {
                ProductId = p.ProductID,
                ProductName = p.ProductName,
                QuantityPerUnit = p.QuantityPerUnit,
                UnitPrice = (decimal) p.UnitPrice
            }).ToList();
        }

        public IEnumerable<ProductInfo> GetProductsByCategory(int categoryID)
        {
            var products = db.Products.Where(p => p.CategoryID == categoryID && p.Discontinued == false);

            return products.Select(p => new ProductInfo()
            {
                ProductId = p.ProductID,
                ProductName = p.ProductName,
                QuantityPerUnit = p.QuantityPerUnit,
                UnitPrice = (decimal)p.UnitPrice
            }).ToList();
        }
    }
}
