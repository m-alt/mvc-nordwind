﻿using Nordwind.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nordwind.Data.Domain.Repository
{
    public class CategoryRepository
    {
        NordwindCtx db;

        public CategoryRepository(NordwindCtx db)
        {
            this.db = db;
        }

        public IEnumerable<Category> GetAll()
        {
            return db.Categories.ToList();
        }
    }
}
