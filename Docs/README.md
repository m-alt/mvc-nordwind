# Project: Nordwind

> MVC Demo Application

## Projekt-Aufbau

1. Leere Solution anlegen
2. Empty ASP.NET Projekt (MVC enabled) mit Test-Projekt anlegen
3. Areas angelegt
4. ViewStart-Dateien in den Areas anlegen (verhindern Scaffolding)
5. Erste Controller und Views um Linking der Actions zu demonstrieren
6. Seperates Datalayer-Projekt anlegen
7. Code-First Modell angelegt, EntityFramework aktualisert.
8. Connection-String in das Web-Projekt kopiert.
