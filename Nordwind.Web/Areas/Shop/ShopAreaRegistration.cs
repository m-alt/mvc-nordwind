﻿using System.Web.Mvc;

namespace Nordwind.Web.Areas.Shop
{
    public class ShopAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Shop";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Homepage",
                "",
                new { controller="Default", action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "Katalog",
                "katalog",
                new { controller = "Catalog", action = "Index", id = UrlParameter.Optional }
            );

            context.MapRoute(
                "Shop_default",
                "Shop/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}