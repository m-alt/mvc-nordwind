﻿using Nordwind.Data.Domain.Repository;
using Nordwind.Data.Model;
using Nordwind.Mvc.Extensions;
using Nordwind.Mvc.Filter;
using Nordwind.Web.Areas.Shop.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nordwind.Web.Areas.Shop.Controllers
{
    [InjectingCategories]
    public class CatalogController : Controller
    {
        NordwindCtx ctx;
        ProductRepository repository;

        public CatalogController()
        {
            ctx = new NordwindCtx();
            repository = new ProductRepository(ctx);
        }

        // GET: Shop/Catalog
        public ActionResult Index()
        {
            return View(repository.GetRandomProducts());
        }

        public ActionResult ProductsByCategory(string slug)
        {
            // ModelBinder and/or injecting ctx?
            var categories = (IEnumerable<Category>)ViewBag.Categories;
            var category = categories.Where(c => c.CategoryName.Slugify() == slug).FirstOrDefault();

            var categoryId = category?.CategoryID ?? -1;
            var products = repository.GetProductsByCategory(categoryId);

            return View(new CatalogViewModel() {
                CurrentCategory = category,
                Products = products
            });
        }
    }
}