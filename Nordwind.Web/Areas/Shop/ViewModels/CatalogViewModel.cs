﻿using Nordwind.Data.Domain;
using Nordwind.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nordwind.Web.Areas.Shop.ViewModels
{
    public class CatalogViewModel
    {
        public Category CurrentCategory { get; set; }
        public IEnumerable<ProductInfo> Products { get; set; }
    }
}