﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nordwind.Web.Areas.Backend.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Backend/Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}